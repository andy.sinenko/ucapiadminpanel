
  CREATE TABLE "UKRAPI"."TUSERINFO" 
   (	"ID" NUMBER(*,0) NOT NULL ENABLE, 
	"NAME" VARCHAR2(45 BYTE) NOT NULL ENABLE, 
	"PASS" VARCHAR2(45 BYTE) NOT NULL ENABLE, 
	"PERMISSIONS" VARCHAR2(255 BYTE), 
	"TYPE" NUMBER(*,0) DEFAULT 1, 
	"UNAME" VARCHAR2(255 BYTE), 
	 CONSTRAINT "TUSERINFO_PK" PRIMARY KEY ("ID")
  ) ;
  
  COMMENT ON COLUMN "UKRAPI"."TUSERINFO"."ID" IS 'Уникальный идентификатор пользователя';
  COMMENT ON COLUMN "UKRAPI"."TUSERINFO"."NAME" IS 'Логин пользователя';
  COMMENT ON COLUMN "UKRAPI"."TUSERINFO"."PASS" IS 'md5 хеш  пароля пользователя';
  COMMENT ON COLUMN "UKRAPI"."TUSERINFO"."PERMISSIONS" IS 'Разрешения пользователя (перечисленные через '','' FIIDы , изменение настроек которого доступны данному пользователю)';
  COMMENT ON COLUMN "UKRAPI"."TUSERINFO"."TYPE" IS 'Тип пользователя(1-администратор системы, 0-пользователь )';
  COMMENT ON COLUMN "UKRAPI"."TUSERINFO"."UNAME" IS 'Полное имя пользователя для отображения внутри интерфейса';
  COMMENT ON TABLE "UKRAPI"."TUSERINFO"  IS 'Пользователи веб-интерфейса администрирования системы MoneyTransfer';
INSERT
INTO TUSERINFO
  ( ID, NAME, PASS, PERMISSIONS, TYPE, UNAME)  VALUES  ( 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ALL', 1, 'Administrator');