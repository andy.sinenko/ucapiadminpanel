<?xml version="1.0" encoding="UTF-8"?>
<persistence version="2.1" xmlns="http://xmlns.jcp.org/xml/ns/persistence"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">


        <persistence-unit name="ukrapi" transaction-type="RESOURCE_LOCAL">
        <!--<persistence-unit name="ukrapi" transaction-type="JTA">-->
            <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
            <!--<non-jta-data-source>java:/comp/env/jdbc/ukrapiDS</non-jta-data-source>-->
            <jta-data-source>java:/comp/env/jdbc/ukrapiDS</jta-data-source>
            <class>com.ukrcard.ukrapiadminpanel.model.entity.Userinfo</class>
            <class>com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem</class>
            <class>com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystemShort</class>
            <class>com.ukrcard.ukrapiadminpanel.model.entity.TRight</class>
            <class>com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos</class>
            <class>com.ukrcard.ukrapiadminpanel.model.entity.TReferenceGroup</class>
            <class>com.ukrcard.ukrapiadminpanel.model.entity.pk.TRightEntityPK</class>
            <exclude-unlisted-classes>true</exclude-unlisted-classes>
            <properties>
                <!--<property name="javax.persistence.nonJtaDataSource" value="java:/comp/env/jdbc/ukrapiDS"/>-->
                <property name="hibernate.generate_statistics" value="false"/>
                <property name="hibernate.id.new_generator_mappings" value="false"/>
                <property name="hibernate.show_sql" value="false"/>
                <property name="hibernate.format_sql" value="false"/>
                <property name="hibernate.timeout" value="0"/>
                <property name="hibernate.dialect" value="org.hibernate.dialect.Oracle10gDialect"/>
                <property name="hibernate.cashable" value="false"/>
            </properties>
        </persistence-unit>

</persistence>
