package com.ukrcard.ukrapiadminpanel.model.repo;

import com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by a.sinenko on 09.03.2016.
 */
@Repository
public interface TSettingsMposRepository extends JpaRepository<TSettingsMpos, BigDecimal> {
    @Query(value = "SELECT t FROM TSettingsMpos t WHERE t.extSystemCode=:extCode")
    List<TSettingsMpos> findAndExternalSystem_ExtCode(@Param("extCode") Integer extCode);

    @Query(value = "SELECT MAX(t.id)+1 FROM  TSettingsMpos t ")
    Long findNextCode();
}
