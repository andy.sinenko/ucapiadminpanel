package com.ukrcard.ukrapiadminpanel.model.repo;

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystemShort;
import com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by a.sinenko on 09.03.2016.
 */

@Repository
public interface ExternalSystemRepository extends JpaRepository<ExternalSystem, BigDecimal> {
   // @Query(value = "SELECT t FROM TRight t ")
    List<ExternalSystem> findAll();

    @Query(value = "SELECT t FROM ExternalSystem t where t.extCode=:extcode")
    List<ExternalSystem> findByCode(@Param("extcode") Integer extcode);

    @Query(value = "SELECT t.extCode FROM ExternalSystem t order by t.extCode")
    List<Integer> findExtCodeOrderByExtCode();

    @Query(value = "SELECT tt.extCode, tt.extId FROM ExternalSystemShort tt order by tt.extCode")
    List<ExternalSystem> findExtCodeAndExtIdOrderByExtCode();

    @Query(value = "SELECT MAX(tt.extCode)+1 FROM ExternalSystemShort tt")
    Integer findNextCode();

    @Query(value = "SELECT t.authKey FROM ExternalSystem t WHERE t.authKey=:authkey")
    List<ExternalSystem> findforUniqueAuthKey(@Param("authkey") String authkey);
}
