package com.ukrcard.ukrapiadminpanel.model.repo;

import com.ukrcard.ukrapiadminpanel.model.entity.TRight;
import com.ukrcard.ukrapiadminpanel.model.entity.pk.TRightEntityPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by a.sinenko on 09.03.2016.
 */
@Repository
public interface TRightRepository extends JpaRepository<TRight, TRightEntityPK> {
   @Query(value = "SELECT t FROM TRight t WHERE t.tRightEntityPK.extSystemCode=:extCode")
    List<TRight> findAndExternalSystem_ExtCode(@Param("extCode") Integer extCode);
}
