package com.ukrcard.ukrapiadminpanel.model.repo;

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.TReferenceGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by a.sinenko on 09.03.2016.
 */

@Repository
public interface ReferenceGroupRepository extends JpaRepository<TReferenceGroup, BigDecimal> {
    @Query(value = "SELECT t.objectCode FROM TReferenceGroup t order by t.objectCode")
    List<Long> findObjectCodeOrderByObjectCode();

    @Query(value = "SELECT t.action FROM TReferenceGroup t where t.objectCode=:objcode")
    String findActionByObjectCode(@Param("objcode") Integer objcode);

    @Query(value = "SELECT MAX(t.objectCode)+1 FROM TReferenceGroup t")
    Integer findNextCode();

}
