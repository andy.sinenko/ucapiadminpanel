package com.ukrcard.ukrapiadminpanel.model.service;

import com.ukrcard.ukrapiadminpanel.model.entity.TRight;
import com.ukrcard.ukrapiadminpanel.model.repo.TRightRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.List;

/**
 * Created by a.sinenko on 11.03.2016.
 */
@Service(value="TRightServiceImpl")
public class TRightServiceImpl implements TRightService {

    Logger logger = LoggerFactory.getLogger(TRightServiceImpl.class);

    @Autowired
    //@Qualifier("TRightRepository")
    private TRightRepository tRightRepository;

    public TRightServiceImpl() {
        //tRightRepository = ApplicationContextHolder.getContext().getBean(TRightRepository.class);
    }

    @Override
    /*@Transactional("uapi")*/
    public List<TRight> findAll() throws PersistenceException {
        List<TRight> tright = tRightRepository.findAll();
        if (tright.size() > 0) {
            return tright;
        }
        throw new PersistenceException("User not found.");
    }

    @Override
    public void saveTRight(TRight tRight){
        tRightRepository.save(tRight);
    }

    @Override
    public void deleteTRight(TRight t){
        tRightRepository.delete(t);
    }

    @Override
    public List<TRight> findAndExternalSystem_ExtCode(Integer ExtCode){
        return tRightRepository.findAndExternalSystem_ExtCode(ExtCode);
    }

    public TRightRepository gettRightRepository() {        return tRightRepository;    }
    public void settRightRepository(TRightRepository tRightRepository) {        this.tRightRepository = tRightRepository;    }
}
