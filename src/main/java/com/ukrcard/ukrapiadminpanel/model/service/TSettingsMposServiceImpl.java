package com.ukrcard.ukrapiadminpanel.model.service;

/**
 * Created by a.sinenko on 29.01.2016.
 */

import com.ukrcard.ukrapiadminpanel.model.entity.TRight;
import com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos;
import com.ukrcard.ukrapiadminpanel.model.repo.ExternalSystemRepository;
import com.ukrcard.ukrapiadminpanel.model.repo.TSettingsMposRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service(value="TSettingsMposServiceImpl")
public class TSettingsMposServiceImpl implements TSettingsMposService{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TSettingsMposRepository tSettingsMposRepository;


    @Override
    public List<TSettingsMpos> findAll() {
        return tSettingsMposRepository.findAll();
    }


    @Override
    public void save(TSettingsMpos tSettingsMpos){
        tSettingsMposRepository.save(tSettingsMpos);
    }

    @Override
    public void delete(TSettingsMpos tSettingsMpos){
        tSettingsMposRepository.delete(tSettingsMpos);
    }

    @Override
    public List<TSettingsMpos> findAndExternalSystem_ExtCode(Integer extCode){

            return tSettingsMposRepository.findAndExternalSystem_ExtCode(extCode);
    }

    @Override
    public Long findNextCode(){
        return tSettingsMposRepository.findNextCode();
    }

}
