package com.ukrcard.ukrapiadminpanel.model.service;

import com.ukrcard.ukrapiadminpanel.model.entity.TRight;
import com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by a.sinenko on 11.03.2016.
 */
public interface TSettingsMposService {
        List<TSettingsMpos> findAll();
        void save(TSettingsMpos tSettingsMpos);
        void delete(TSettingsMpos tSettingsMpos);
        List<TSettingsMpos> findAndExternalSystem_ExtCode(Integer extCode);
        Long findNextCode();
}
