package com.ukrcard.ukrapiadminpanel.model.service;

/**
 * Created by a.sinenko on 29.01.2016.
 */
import com.ukrcard.ukrapiadminpanel.model.entity.Userinfo;
import com.ukrcard.ukrapiadminpanel.model.repo.UserInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.math.BigDecimal;
import java.util.List;
@Service(value="UserinfoServiceImpl")
public class UserinfoServiceImpl implements UserinfoService {

    Logger logger = LoggerFactory.getLogger(UserinfoServiceImpl.class);


    @Autowired
    //@Qualifier("userInfoRepository")
    private UserInfoRepository userInfoRepository;


    @Override
    public Userinfo findUserByName(String Username) throws PersistenceException {
        List<Userinfo> userinfo = userInfoRepository.findUserByName(Username);
        if (userinfo.size() > 0) {
            return userinfo.get(0);
        }
        throw new PersistenceException("User not found.");
    }


    @Override
    public List<Userinfo> findUserById(BigDecimal id) throws PersistenceException {
        List<Userinfo> users = userInfoRepository.findUserById(id);
        if (users.size() > 0) {
            return users;
        }
        throw new PersistenceException("User not found.");
    }


    @Override
    public Userinfo login(String name, String pass){
        pass = getHash(pass);
        List<Userinfo> users=null;
        users = userInfoRepository.login(name, pass);
        if (users == null || users.size() == 0) {
            return null;
        } else {
            return users.get(0);
        }
    }

    @Override
    public void save(Userinfo userinfo){
        userInfoRepository.save(userinfo);
    }


    private String getHash(String pass) {
        return org.apache.commons.codec.digest.DigestUtils.md5Hex(pass);
        //return org.apache.commons.codec.digest.DigestUtils.sha256Hex(pass); //стоит ли переделывать на sha256?
    }


    public List<Userinfo> refreshUser(Userinfo userinfo) {
        List<Userinfo> users = userInfoRepository.findUserById(userinfo.getId());
        if (users.size() > 0) {
            return users;
        }
        return null;
    }

    public UserInfoRepository getUserInfoRepository() {
        return userInfoRepository;
    }

    public void setUserInfoRepository(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }
}
