package com.ukrcard.ukrapiadminpanel.model.service;

/**
 * Created by a.sinenko on 09.03.2016.
 */
import com.ukrcard.ukrapiadminpanel.model.entity.Userinfo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

public interface UserinfoService {
    Userinfo findUserByName(String Username);
    Userinfo login(String name, String pass);
    List<Userinfo> findUserById(BigDecimal id);
    void save(Userinfo userinfo);
}
