package com.ukrcard.ukrapiadminpanel.model.service;

import com.ukrcard.ukrapiadminpanel.model.entity.TRight;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by a.sinenko on 11.03.2016.
 */

public interface TRightService {
        List<TRight> findAll();
        void saveTRight(TRight t);
        void deleteTRight(TRight t);
        List<TRight> findAndExternalSystem_ExtCode(Integer ExtCode);
}
