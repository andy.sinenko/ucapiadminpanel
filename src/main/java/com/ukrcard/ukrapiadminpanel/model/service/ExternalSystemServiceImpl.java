package com.ukrcard.ukrapiadminpanel.model.service;

/**
 * Created by a.sinenko on 29.01.2016.
 */

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.repo.ExternalSystemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value="ExternalSystemServiceImpl")
public class ExternalSystemServiceImpl implements ExternalSystemService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private ExternalSystemRepository externalSystemRepository;

    @Override
    public List<ExternalSystem> findAll() {
        return externalSystemRepository.findAll();
    }

    @Override
    public void save(ExternalSystem externalSystem){
        externalSystemRepository.save(externalSystem);
    }

    @Override
    public void delete(ExternalSystem externalSystem){
        externalSystemRepository.delete(externalSystem);
    }

    @Override
    public List<Integer> findExtCodeOrderByExtCode() {
        return externalSystemRepository.findExtCodeOrderByExtCode();
    }

    @Override
    public List<ExternalSystem> findExtCodeAndExtIdOrderByExtCode(){
        return externalSystemRepository.findExtCodeAndExtIdOrderByExtCode();
    }

    @Override
    public List<ExternalSystem> findByCode(Integer extcode){
        return externalSystemRepository.findByCode(extcode);
    }

    @Override
    public Integer findNextCode(){
        return externalSystemRepository.findNextCode();
    }

    @Override
    public List<ExternalSystem> findforUniqueAuthKey(String authkey){
        return externalSystemRepository.findforUniqueAuthKey(authkey);
    }
}
