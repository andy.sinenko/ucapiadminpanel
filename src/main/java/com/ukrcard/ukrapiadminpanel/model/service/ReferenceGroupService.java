package com.ukrcard.ukrapiadminpanel.model.service;

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.TReferenceGroup;

import java.util.List;

/**
 * Created by a.sinenko on 11.03.2016.
 */

public interface ReferenceGroupService {
        List<TReferenceGroup> findAll();
        void save(TReferenceGroup refGroup);
        void delete(TReferenceGroup refGroup);
        List<Long> findObjectCodeOrderByObjectCode();
        String findActionByObjectCode(Integer objcode);
        Integer findNextCode();
}
