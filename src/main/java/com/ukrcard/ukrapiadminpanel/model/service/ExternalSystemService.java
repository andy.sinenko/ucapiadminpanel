package com.ukrcard.ukrapiadminpanel.model.service;

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystemShort;
import com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos;
import com.ukrcard.ukrapiadminpanel.model.repo.ExternalSystemRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by a.sinenko on 11.03.2016.
 */

public interface ExternalSystemService {
        List<ExternalSystem> findAll();
        void save(ExternalSystem externalSystem);
        void delete(ExternalSystem externalSystem);
        List<Integer> findExtCodeOrderByExtCode();
        List<ExternalSystem> findExtCodeAndExtIdOrderByExtCode();
        List<ExternalSystem> findByCode(Integer extcode);
        Integer findNextCode();
        List<ExternalSystem> findforUniqueAuthKey(String authkey);
}
