package com.ukrcard.ukrapiadminpanel.model.service;

/**
 * Created by a.sinenko on 29.01.2016.
 */

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.TReferenceGroup;
import com.ukrcard.ukrapiadminpanel.model.repo.ExternalSystemRepository;
import com.ukrcard.ukrapiadminpanel.model.repo.ReferenceGroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value="ReferenceGroupServiceImpl")
public class ReferenceGroupServiceImpl implements ReferenceGroupService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ReferenceGroupRepository referenceGroupRepository;

    @Override
    public List<TReferenceGroup> findAll() {
        return referenceGroupRepository.findAll();
    }

    @Override
    public void save(TReferenceGroup referenceGroup){
        referenceGroupRepository.save(referenceGroup);
    }

    @Override
    public void delete(TReferenceGroup referenceGroup){
        referenceGroupRepository.delete(referenceGroup);
    }

    @Override
    public List<Long> findObjectCodeOrderByObjectCode() {
        return referenceGroupRepository.findObjectCodeOrderByObjectCode();
    }

    @Override
    public String findActionByObjectCode(Integer objcode){
        return referenceGroupRepository.findActionByObjectCode(objcode);
    }

    @Override
    public Integer findNextCode(){
        return referenceGroupRepository.findNextCode();
    }
}
