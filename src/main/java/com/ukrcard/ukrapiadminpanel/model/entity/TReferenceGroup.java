/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ukrcard.ukrapiadminpanel.model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by a.sinenko on 11.03.2016.
 */
@Entity
@Table(name = "TREFRESOURCEGROUP")
@Cacheable(false)
public class TReferenceGroup implements Serializable {

    //private static final long serialVersionUID = 1L;

    @Column(name = "OBJECTCODE")
    @Id
    private Integer objectCode;

    @Basic(optional = false)
    @Column(name = "OBJECT")
    private String object;

    @Basic(optional = false)
    @Column(name = "DESCRIPTION")
    private String description;

    @Basic(optional = false)
    @Column(name = "ACTION")
    private String action;


    public TReferenceGroup() {
    }

    public TReferenceGroup(Integer objectCode, String object, String description, String action) {
        this.objectCode = objectCode;
        this.object = object;
        this.description = description;
        this.action = action;
    }


    public Integer getObjectCode() {
        return objectCode;
    }

    public String getObject() {
        return object;
    }

    public String getDescription() {
        return description;
    }

    public String getAction() {
        return action;
    }

    public void setObjectCode(Integer objectCode) {
        this.objectCode = objectCode;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "\n    TReferenceGroupEntity{" +
                "objectCode=" + objectCode +
                ", object='" + object + '\'' +
                ", description='" + description + '\'' +
                ", action='" + action + '\'' +
                '}';
    }
}
