package com.ukrcard.ukrapiadminpanel.model.entity;

import com.ukrcard.ukrapiadminpanel.model.entity.pk.TRightEntityPK;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by a.sinenko on 11.03.2016.
 */
@Entity
@Table(name = "TRIGHT")
@Cacheable(false)
@NamedQueries({
        @NamedQuery(name="TRight.findAll", query = "SELECT t from TRight t order by t.tRightEntityPK asc")
})
public class TRight implements Serializable {

    public static final String findAll = "TRight.findAll";

    @EmbeddedId
    private TRightEntityPK tRightEntityPK;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OBJECTCODE", referencedColumnName = "OBJECTCODE", insertable = false, updatable = false)
    private TReferenceGroup tReferenceGroup;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "EXTSYSTEMCODE", referencedColumnName = "EXTSYSTEMCODE", insertable = false, updatable = false)
    private ExternalSystem externalSystem;


    @Basic(optional = false)
    @Column(name = "ACTION")
    private String action;

    @Basic(optional = false)
    @Column(name = "UPDATESYSDATE")
    @Temporal(TemporalType.DATE)
    private Date updateSysDate;

    public TReferenceGroup gettReferenceGroup() {
        return tReferenceGroup;
    }

    public void settReferenceGroup(TReferenceGroup tReferenceGroup) {
        this.tReferenceGroup = tReferenceGroup;
    }

    public TRight() {
    }

    public String getAction() {
        return action;
    }

    public Date getUpdateSysDate() {
        return updateSysDate;
    }


    public void setAction(String action) {
        this.action = action;
    }

    public void setUpdateSysDate(Date updateSysDate) {
        this.updateSysDate = updateSysDate;
    }


    public TRightEntityPK gettRightEntityPK() {
        return tRightEntityPK;
    }

    public void settRightEntityPK(TRightEntityPK tRightEntityPK) {
        this.tRightEntityPK = tRightEntityPK;
    }

    public ExternalSystem getExternalSystem() {
        return externalSystem;
    }

    public void setExternalSystem(ExternalSystem externalSystem) {
        this.externalSystem = externalSystem;
    }

    @Override
    public String toString() {
        return "\n    TRightEntity{" +
                tRightEntityPK +
                ", action='" + action + '\'' +
                ", updateSysDate=" + updateSysDate +
                ",\n    tReferenceGroupEntity=" + /*tReferenceGroupEntity +*/
                '}';
    }
}
