package com.ukrcard.ukrapiadminpanel.model.entity;

/**
 * Created by a.sinenko on 30.03.2016.
 */
public class AuthorizedActions {
    private String Item;
    private String Description;

    public AuthorizedActions() {
        super();
        Description=null;
        Item = null;
    }

    public AuthorizedActions(String description, String item){
        Item = item;
        Description = description;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }
}
