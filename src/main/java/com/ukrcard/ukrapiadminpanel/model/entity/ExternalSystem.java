package com.ukrcard.ukrapiadminpanel.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by a.sinenko on 11.03.2016.
 */
@Entity
@NamedStoredProcedureQuery(name = "p_uapi_pkg.getExtSystem", procedureName = "p_uapi_pkg.getExtSystem", resultClasses = ExternalSystem.class,
        parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "m_extSystemId", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.REF_CURSOR,name="c_result",type=ExternalSystem.class)})

@Table(name = "TEXTERNALSYSTEM", uniqueConstraints= @UniqueConstraint(columnNames={"EXTSYSTEMCODE", "AUTHKEY"}))

@Cacheable(false)
public class ExternalSystem  implements Serializable {

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "EXTSYSTEMCODE", referencedColumnName = "EXTSYSTEMCODE")
    private List<TRight> TRightList;

    @Column(name = "EXTSYSTEMCODE")
    @Id
    private Integer extCode;

    @Column(name = "EXTSYSTEMID")
    private String extId;

    @Basic(optional = false)
    @Column(name = "DESCRIPTION")
    private String description;

    @Basic(optional = false)
    @Temporal(TemporalType.DATE)
    @Column(name = "REGSYSDATE")
    private Date regSysDate;

    @Basic(optional = false)
    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATESYSDATE")
    private Date updateSysDate;

    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;

    @Basic(optional = false)
    @Column(name = "FLAGAPPAUTH")
    private String flagAppAuth;

    @Basic(optional = false)
    @Column(name = "AUTHKEY")
    private String authKey;

    @Basic(optional = false)
    @Column(name = "PSID")
    private String psid;

    @Basic(optional = false)
    @Column(name = "FIID")
    private String fiid;

    @Basic(optional = false)
    @Column(name = "HMACKEY")
    private String hmacKey;

    public List<TRight> getTRightList() {
        return TRightList;
    }

    public Integer getExtCode() {
        return extCode;
    }

    public String getExtId() {
        return extId;
    }

    public String getDescription() {
        return description;
    }

    public Date getRegSysDate() {
        return regSysDate;
    }

    public Date getUpdateSysDate() {
        return updateSysDate;
    }

    public String getStatus() {
        return status;
    }

    public String getFlagAppAuth() {
        return flagAppAuth;
    }

    public String getAuthKey() {
        return authKey;
    }

    public String getPsid() {
        return psid;
    }

    public String getFiid() {
        return fiid;
    }

    public void setTRightList(List<TRight> TRightList) {
        this.TRightList = TRightList;
    }

    public void setExtCode(Integer extCode) {
        this.extCode = extCode;
    }

    public void setExtId(String extId) {
        this.extId = extId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRegSysDate(Date regSysDate) {
        this.regSysDate = regSysDate;
    }

    public void setUpdateSysDate(Date updateSysDate) {
        this.updateSysDate = updateSysDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setFlagAppAuth(String flagAppAuth) {
        this.flagAppAuth = flagAppAuth;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setPsid(String psid) {
        this.psid = psid;
    }

    public void setFiid(String fiid) {
        this.fiid = fiid;
    }

    public String getHmacKey() {
        return hmacKey;
    }

    public void setHmacKey(String hmacKey) {
        this.hmacKey = hmacKey;
    }

    @Override
    public String toString() {
        return "ExternalSystemEntity{" +
                ", extCode=" + extCode +
                ", extId='" + extId + '\'' +
                ", description='" + description + '\'' +
                ", regSysDate=" + regSysDate +
                ", updateSysDate=" + updateSysDate +
                ", status='" + status + '\'' +
                ", flagAppAuth='" + flagAppAuth + '\'' +
                ", authKey='" + authKey + '\'' +
                ", psid='" + psid + '\'' +
                ", fiid='" + fiid + '\'' +
                ",\n    TRightList=" + TRightList +
                '}';
    }
}
