/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ukrcard.ukrapiadminpanel.model.entity.pk;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by a.sinenko on 11.03.2016.
 */
@Embeddable
public class TRightEntityPK implements Serializable {


    @Column(name = "EXTSYSTEMCODE", nullable = false)
    private Integer extSystemCode;

    @Column(name = "OBJECTCODE", nullable = false)
    private Integer objectCode;

    public TRightEntityPK() {
    }

    public TRightEntityPK(Integer extSystemCode, Integer objectCode) {
        this.extSystemCode = extSystemCode;
        this.objectCode = objectCode;
    }

    public Integer getExtSystemCode() {
        return extSystemCode;
    }

    public void setExtSystemCode(Integer extSystemCode) {
        this.extSystemCode = extSystemCode;
    }

    public Integer getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(Integer objectCode) {
        this.objectCode = objectCode;
    }

    @Override
    public String toString() {
        return "TRightEntityPK{" +
                "extSystemCode=" + extSystemCode +
                ", objectCode=" + objectCode +
//                ", referenceGroupEntity=" + referenceGroupEntity +
                '}';
    }
}
