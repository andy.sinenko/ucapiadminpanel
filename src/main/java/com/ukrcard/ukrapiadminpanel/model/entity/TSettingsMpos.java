package com.ukrcard.ukrapiadminpanel.model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by a.sinenko on 11.03.2016.
 */
@Entity
@Table(name = "TSETTINGS_MPOS")

@NamedStoredProcedureQuery(name = "getFreePos",
        procedureName = "p_uapi_pkg.getFreePos",resultClasses = TSettingsMpos.class,
        parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "m_extSystemCode", type = Integer.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "m_psid", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "m_operation", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "c_result", type =  void.class)
                //@StoredProcedureParameter(mode = ParameterMode.OUT, name = "c_result", type = TSettingsMposEntity.class)
                //,@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "t_cursor", type= TSettingsMposEntity.class)
        })
@Cacheable(false)
public class TSettingsMpos implements Serializable {

    @Column(name = "ID")
/*    @GeneratedValue(strategy = GenerationType.AUTO)*/
    @Id
    private Long id;

    @Basic(optional = false)
    @Column(name = "TERMID")
    private String termId;

    @Basic(optional = true)
    @Column(name = "EXTSYSTEMCODE")
    private Integer extSystemCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "EXTSYSTEMCODE", referencedColumnName = "EXTSYSTEMCODE", insertable = false, updatable = false)
    private ExternalSystem externalSystem;

    @Basic(optional = false)
    @Column(name = "FIID")
    private String fiid;

    @Basic(optional = false)
    @Column(name = "PSID")
    private String psid;

    @Basic(optional = false)
    @Column(name = "TERMIP")
    private String termIP;

    @Basic(optional = false)
    @Column(name = "TERMPORT")
    private String termport;


    @Basic(optional = false)
    @Column(name = "TIMEOUT")
    private Integer timeOut;


    @Basic(optional = true)
    @Column(name = "ISBUSY")
    private Integer isBusy;


    @Basic(optional = false)
    @Column(name = "OPERATION")
    private String operation;


    @Basic(optional = false)
    @Column(name = "PSID_IssAlwd")
    private String psidIsAlwd;


    public TSettingsMpos() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public Integer getExtSystemCode() {
        return extSystemCode;
    }

    public void setExtSystemCode(Integer extSystemCode) {
        this.extSystemCode = extSystemCode;
    }

    public String getFiid() {
        return fiid;
    }

    public void setFiid(String fiid) {
        this.fiid = fiid;
    }

    public String getPsid() {
        return psid;
    }

    public void setPsid(String psid) {
        this.psid = psid;
    }

    public String getTermIP() {
        return termIP;
    }

    public void setTermIP(String termIP) {
        this.termIP = termIP;
    }

    public String getTermport() {
        return termport;
    }

    public void setTermport(String termport) {
        this.termport = termport;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }

    public Integer getIsBusy() {
        return isBusy;
    }

    public void setIsBusy(Integer isBusy) {
        this.isBusy = isBusy;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getPsidIsAlwd() {
        return psidIsAlwd;
    }

    public void setPsidIsAlwd(String psidIsAlwd) {
        this.psidIsAlwd = psidIsAlwd;
    }

    public ExternalSystem getExternalSystem() {
        return externalSystem;
    }

    public void setExternalSystem(ExternalSystem externalSystem) {
        this.externalSystem = externalSystem;
    }

    @Override
    public String toString() {
        return "TSettingsMposEntity{" +
                "id=" + id +
                ", termId='" + termId + '\'' +
                ", extSystemCode='" + extSystemCode + '\'' +
                ", fiid='" + fiid + '\'' +
                ", psid='" + psid + '\'' +
                ", termIP='" + termIP + '\'' +
                ", termport='" + termport + '\'' +
                ", termOut=" + timeOut +
                ", isBusy=" + isBusy +
                ", operation='" + operation + '\'' +
                ", psidIsAlwd='" + psidIsAlwd + '\'' +
                '}';
    }
}
