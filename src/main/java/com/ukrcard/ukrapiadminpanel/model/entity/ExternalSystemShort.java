package com.ukrcard.ukrapiadminpanel.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by a.sinenko on 08.10.2015.
 */

@Entity
@Table(name = "TEXTERNALSYSTEM")
@Cacheable(false)
public class ExternalSystemShort implements Serializable {

    @Column(name = "EXTSYSTEMCODE")
    @Id
    private Integer extCode;

    @Column(name = "EXTSYSTEMID")
    private String extId;


    public Integer getExtCode() {
        return extCode;
    }

    public String getExtId() {
        return extId;
    }


    public void setExtCode(Integer extCode) {
        this.extCode = extCode;
    }

    public void setExtId(String extId) {
        this.extId = extId;
    }

    @Override
    public String toString() {
        return  extCode + " - " + extId;
    }
}
