package com.ukrcard.ukrapiadminpanel.view;

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos;
import com.ukrcard.ukrapiadminpanel.model.service.ExternalSystemService;
import com.ukrcard.ukrapiadminpanel.model.service.TSettingsMposService;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessResourceUsageException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.sinenko on 01.03.2016.
 */
@ManagedBean(name = "externalsystemview")
@SessionScoped
public class ExternalSystemView implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ManagedProperty(value = "#{trightview}")
    TRightView tRightView;

    @ManagedProperty(value = "#{tsettingsmposview}")
    TSettingsMposView settingsMposView;

    @ManagedProperty(value = "#{ExternalSystemServiceImpl}")
    ExternalSystemService externalSystemDAO;

    List<ExternalSystem> externalSystemList;

    ExternalSystem concreetExternalSystem;

    int activeTabIndex = 0;
    int activeTabIndex1 = 0;

    public int getActiveTabIndex1() {
        return activeTabIndex1;
    }

    public void setActiveTabIndex1(int activeTabIndex1) {
        this.activeTabIndex1 = activeTabIndex1;
    }


    public ExternalSystemService getExternalSystemDAO() {
        return externalSystemDAO;
    }

    public void setExternalSystemDAO(ExternalSystemService externalSystemDAO) {
        this.externalSystemDAO = externalSystemDAO;
    }

    public List<ExternalSystem> getExternalSystemList() {
        return externalSystemList;
    }

    public void setExternalSystemList(List<ExternalSystem> externalSystemList) {
        this.externalSystemList = externalSystemList;
    }

    public ExternalSystem getConcreetExternalSystem() {
        return concreetExternalSystem;
    }

    public void setConcreetExternalSystem(ExternalSystem concreetExternalSystem) {
        this.concreetExternalSystem = concreetExternalSystem;
    }

    public TRightView gettRightView() {
        return tRightView;
    }

    public void settRightView(TRightView tRightView) {
        this.tRightView = tRightView;
    }

    public TSettingsMposView getSettingsMposView() {
        return settingsMposView;
    }

    public void setSettingsMposView(TSettingsMposView settingsMposView) {
        this.settingsMposView = settingsMposView;
    }

    public int getActiveTabIndex() {
        return activeTabIndex;
    }

    public void setActiveTabIndex(int activeTabIndex) {
        this.activeTabIndex = activeTabIndex;
    }

    @PostConstruct
    public void init() {
        reinitializeTable();
        loadData();
    }

    public void reinitializeTable() {//reinitialize table and clear selection of row
        concreetExternalSystem = null;
        tRightView.concreeteExternalSystem = concreetExternalSystem;
        settingsMposView.concreeteExternalSystem = concreetExternalSystem;
    }

    public void loadData() {
        settingsMposView.loadData();
        tRightView.loadData();
        try{
            externalSystemList = externalSystemDAO.findAll();
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    public void close() {
        reinitializeTable();
        RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Действие отменено"));
    }

    public void showEditDialog(ExternalSystem ExternalSystemItem) {
        concreetExternalSystem = ExternalSystemItem;
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("closable", true);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/externalsystemdialog", options, null);
    }

    public void deleteRecord(ExternalSystem ExternalSystemItem) {
        externalSystemDAO.delete(ExternalSystemItem);
        externalSystemList.remove(ExternalSystemItem);
        reinitializeTable();
        loadData();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Успешно удалено"));
    }


    public boolean saveFocusedRow() {
        concreetExternalSystem.setExtId(concreetExternalSystem.getExtId().trim());
        concreetExternalSystem.setFiid(concreetExternalSystem.getFiid().trim());
        concreetExternalSystem.setStatus(concreetExternalSystem.getStatus().trim());
        concreetExternalSystem.setFlagAppAuth(concreetExternalSystem.getFlagAppAuth().trim());
        concreetExternalSystem.setPsid(concreetExternalSystem.getPsid().trim());
        if(checkForUniqueAuthKey(concreetExternalSystem.getAuthKey().trim()))
            concreetExternalSystem.setAuthKey(concreetExternalSystem.getAuthKey().trim());
        concreetExternalSystem.setAuthKey(concreetExternalSystem.getAuthKey().trim());
        concreetExternalSystem.setHmacKey(concreetExternalSystem.getHmacKey().trim());

        if (concreetExternalSystem.getExtId().length() != 0 &&
                concreetExternalSystem.getFiid().length() != 0 &&
                concreetExternalSystem.getPsid().length() != 0
                && checkForUniqueAuthKey(concreetExternalSystem.getAuthKey().trim())) {
            try {
                externalSystemDAO.save(concreetExternalSystem);
            } catch (DataAccessException err) {
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
            }
            reinitializeTable();
            loadData();
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Успешно", "Запись сохранёнена"));
        } else {
            reinitializeTable();
            loadData();
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Одно из обязательных полей не заполнено или Ключ Аутентификации не уникален."));
        }

        return true;
    }

    boolean checkForUniqueAuthKey(String key){
        List<ExternalSystem> temp = null;
        temp = externalSystemDAO.findforUniqueAuthKey(key);
        if(temp.size() > 0)
            return false;
        return true;
    }

    public void showAddNewDialog() {
        Integer extCode;
        try {
            extCode = externalSystemDAO.findNextCode();

            concreetExternalSystem = new ExternalSystem();
            concreetExternalSystem.setExtCode(extCode);
            concreetExternalSystem.setRegSysDate(new Date());
            concreetExternalSystem.setUpdateSysDate(new Date());
            concreetExternalSystem.setFiid("0000");
            concreetExternalSystem.setPsid("0000");
            concreetExternalSystem.setFlagAppAuth("1");
            concreetExternalSystem.setStatus("0");
            Map<String, Object> options = new HashMap<String, Object>();
            options.put("closable", false);
            options.put("resizable", false);
            options.put("contentWidth", 600);
            RequestContext.getCurrentInstance().openDialog("dialogs/externalsystemdialog", options, null);
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    public void onDialogClose(SelectEvent event) {
        reinitializeTable();
        loadData();
        FacesMessage message = (FacesMessage) event.getObject();
        if (message != null)
            FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void onRowSelect(SelectEvent event) {
        tRightView.concreeteExternalSystem = concreetExternalSystem;
        settingsMposView.concreeteExternalSystem = concreetExternalSystem;
        //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Внимание!", "Внешняя система." + ((ExternalSystem) event.getObject()).getExtId()));
        settingsMposView.refreshRightList(((ExternalSystem) event.getObject()).getExtCode());
        tRightView.refreshRightList(((ExternalSystem) event.getObject()).getExtCode());
        RequestContext.getCurrentInstance().update(":tabview");
        RequestContext.getCurrentInstance().update(":tabview1");
    }

    public void showEditContextMenu() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("closable", true);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        /*TabView tabView = (TabView)FacesContext.getCurrentInstance().getViewRoot().findComponent("tabview");
        tabView.setActiveIndex(0);*/
        RequestContext.getCurrentInstance().openDialog("dialogs/externalsystemdialog", options, null);
    }

    public void onChangeTab(TabChangeEvent event) {
        TabView tv = (TabView) event.getComponent();
        this.activeTabIndex = tv.getChildren().indexOf(event.getTab());
    }

    public void onChangeTab1(TabChangeEvent event) {
        TabView tv = (TabView) event.getComponent();
        this.activeTabIndex1 = tv.getChildren().indexOf(event.getTab());
    }
}
