package com.ukrcard.ukrapiadminpanel.view;

import com.ukrcard.ukrapiadminpanel.controller.ApplicationUser;
import com.ukrcard.ukrapiadminpanel.model.entity.Userinfo;
import com.ukrcard.ukrapiadminpanel.model.service.UserinfoService;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.sinenko on 11.03.2016.
 */
@ManagedBean(name = "user")
@ViewScoped
public class User implements Serializable{

    Logger logger = LoggerFactory.getLogger(User.class);

    @ManagedProperty(value="#{UserinfoServiceImpl}")
    private UserinfoService userDAO;

    private Userinfo userInfo;

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{applicationUser}")
    private ApplicationUser applicationUser;

    String password1;
    String password2;

    @PostConstruct
    void postConstruct(){
        try{
            BigDecimal b = applicationUser.getUserInfo().getId();
            List<Userinfo> temp = userDAO.findUserById(b);
            userInfo = temp.get(0);
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public ApplicationUser getApplicationUser() {
        return applicationUser;
    }

    public void setApplicationUser(ApplicationUser applicationUser) {
        this.applicationUser = applicationUser;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public UserinfoService getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserinfoService userDAO) {
        this.userDAO = userDAO;
    }

    public Userinfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(Userinfo userInfo) {
        this.userInfo = userInfo;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private String getHash(String pass) {
        return org.apache.commons.codec.digest.DigestUtils.md5Hex(pass);
        //return org.apache.commons.codec.digest.DigestUtils.sha256Hex(pass); //стоит ли переделывать на sha256?
    }

    public void close() {
        RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Действие отменено"));
    }

    public boolean saveFocusedRow(){
        if(password1.equals(password2)) {
            String pass = getHash(password1);
            userInfo.setPass(pass);
            try {
                userDAO.save(userInfo);
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Успешно", "Пароль сохранен."));
            } catch (DataAccessException err) {
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
            }
        }
        else
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Введенные части пароля не совпадают"));

        return true;
    }

    public void editPassword(ActionEvent actionEvent){
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("closable", true);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/changepassworddialog", options, null);
    }

}
