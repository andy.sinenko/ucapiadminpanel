package com.ukrcard.ukrapiadminpanel.view;

/**
 * Created by a.sinenko on 02.03.2016.
 */

import com.ukrcard.ukrapiadminpanel.model.entity.AuthorizedActions;
import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.TReferenceGroup;
import com.ukrcard.ukrapiadminpanel.model.entity.TRight;
import com.ukrcard.ukrapiadminpanel.model.entity.pk.TRightEntityPK;
import com.ukrcard.ukrapiadminpanel.model.service.ExternalSystemService;
import com.ukrcard.ukrapiadminpanel.model.service.ReferenceGroupService;
import com.ukrcard.ukrapiadminpanel.model.service.TRightService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.*;

@ManagedBean(name="trightview")
@SessionScoped
public class TRightView implements Serializable{

    private static final long serialVersionUID = 1L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

/*    @Autowired
    @Qualifier("TRightServiceImpl")
     transient private TRightService tRightDAO;*/

    @ManagedProperty(value="#{TRightServiceImpl}")
    private TRightService tRightDAO;

    @ManagedProperty(value="#{ExternalSystemServiceImpl}")
    private ExternalSystemService externalSystemDAO;

    @ManagedProperty(value="#{ReferenceGroupServiceImpl}")
    private ReferenceGroupService referenceGroupDAO;

    ExternalSystem concreeteExternalSystem;

    TReferenceGroup concreteReferenceGroup;

    private List<Long> objectCodeGroupList;

    private List<TReferenceGroup> tReferenceGroupList;

    private List<ExternalSystem> externalSystemList;

    private List<Integer> externalCodeList;

    private List<TRight> tRightList;

    private List<AuthorizedActions> authorizedActionsList = Arrays.asList(new AuthorizedActions("S – информационные запросы", "S"),
            new AuthorizedActions("M – запросы на модификацию", "M"),
            new AuthorizedActions("SM - разрешены и информационные, и запросы на модификацию", "SM")
    );

    private TRight concreetRightView;

    private String Actions;

    public List<ExternalSystem> getExternalSystemList() {
        return externalSystemList;
    }

    public void setExternalSystemList(List<ExternalSystem> externalSystemList) {
        this.externalSystemList = externalSystemList;
    }

    public List<TReferenceGroup> gettReferenceGroupList() {
        return tReferenceGroupList;
    }

    public void settReferenceGroupList(List<TReferenceGroup> tReferenceGroupList) {
        this.tReferenceGroupList = tReferenceGroupList;
    }

    public TReferenceGroup getConcreteReferenceGroup() {
        return concreteReferenceGroup;
    }

    public void setConcreteReferenceGroup(TReferenceGroup concreteReferenceGroup) {
        this.concreteReferenceGroup = concreteReferenceGroup;
    }

    public ExternalSystem getConcreeteExternalSystem() {
        return concreeteExternalSystem;
    }

    public void setConcreeteExternalSystem(ExternalSystem concreeteExternalSystem) {
        this.concreeteExternalSystem = concreeteExternalSystem;
    }

    public TRight getConcreetRightView() {
        return concreetRightView;
    }

    public void setConcreetRightView(TRight concreetRightView) {
        this.concreetRightView = concreetRightView;
    }

    public List<TRight> gettRightList() {
        return tRightList;
    }

    public void settRightList(List<TRight> tRightList) {
        this.tRightList = tRightList;
    }

    public void refreshRightList(Integer extCode)    {
        List<TRight> tempList = tRightDAO.findAndExternalSystem_ExtCode(extCode);
        if(tempList!=null && tempList.size() > 0)
            this.tRightList = tempList;
        else
            this.tRightList = null;
        //RequestContext.getCurrentInstance().update(":tabview:form:trighttable");
        RequestContext.getCurrentInstance().update("trighttable");

    }

    public String getActions() {
        return Actions;
    }

    public void setActions(String actions) {
        Actions = actions;
    }

    @PostConstruct
    private void init(){
        loadData();
    }

    public void loadData(){
        tReferenceGroupList = referenceGroupDAO.findAll();
        //objectCodeGroupList = referenceGroupDAO.findObjectCodeOrderByObjectCode();
        externalSystemList = externalSystemDAO.findAll();
        //tRightList = tRightDAO.findAll();
        //tRightList = null;
        if(concreeteExternalSystem != null)
            try {
                tRightList = tRightDAO.findAndExternalSystem_ExtCode(concreeteExternalSystem.getExtCode());
            } catch (DataAccessException err) {
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
            }
    }


    public TRightService gettRightDAO() {        return tRightDAO;    }
    public void settRightDAO(TRightService tRightDAO) {        this.tRightDAO = tRightDAO;    }

    public void close() {
        RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Действие отменено"));

    }


    public void showEditTRight(TRight TRightItem){
        concreetRightView = TRightItem;
        serviceChange();
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("closable", true);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/trightdialog",options,null);
    }

    public void deleteRight(TRight TRightItem){
        tRightDAO.deleteTRight(TRightItem);
        tRightList.remove(TRightItem);
        loadData();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Удаление записи", "Успешно удалено"));
    }


    public void saveFocusedTright(ActionEvent actionEvent){
        concreetRightView.setUpdateSysDate(new Date());
        externalSystemList = externalSystemDAO.findByCode(concreeteExternalSystem.getExtCode());
        tRightDAO.saveTRight(concreetRightView);
        loadData();
        RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Успешно", "Запись сохранёнена"));
        //return true;
    }

    public void showAddNewDialog(){
        if(concreeteExternalSystem != null) {
            externalSystemList = externalSystemDAO.findByCode(concreeteExternalSystem.getExtCode());
            concreetRightView = new TRight();
            Integer extcode = concreeteExternalSystem.getExtCode();
            concreetRightView.setUpdateSysDate(new Date());
            TRightEntityPK tRightEntityPK = new TRightEntityPK();
            tRightEntityPK.setExtSystemCode(extcode);
            if(concreteReferenceGroup != null)
                tRightEntityPK.setObjectCode(concreteReferenceGroup.getObjectCode());
            else
                tRightEntityPK.setObjectCode(100);
            concreetRightView.settRightEntityPK(tRightEntityPK);
            serviceChange();
            Map<String, Object> options = new HashMap<String, Object>();
            options.put("closable", true);
            options.put("resizable", false);
            options.put("contentWidth", 600);
            RequestContext.getCurrentInstance().openDialog("dialogs/trightdialog", options, null);
        }
        else{
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Внимание!", "Для добавления Прав доступа к внешней системе выберите соотв. систему и объект. на ввкладнках 'Внешние системы' и 'Ресурсы UAPI'"));
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Внимание!", "Для добавления Прав доступа к внешней системе выберите соотв. систему на вкладке 'Внешние системы'"));
        }
    }

    public void onDialogClose(SelectEvent event){
        FacesMessage message = (FacesMessage) event.getObject();
        if (message != null)
            FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<AuthorizedActions> getAuthorizedActionsList() {
        return authorizedActionsList;
    }

    public void setAuthorizedActionsList(List<AuthorizedActions> authorizedActionsList) {
        this.authorizedActionsList = authorizedActionsList;
    }

    public ExternalSystemService getExternalSystemDAO() {
        return externalSystemDAO;
    }

    public void setExternalSystemDAO(ExternalSystemService externalSystemDAO) {
        this.externalSystemDAO = externalSystemDAO;
    }

    public List<Integer> getExternalCodeList() {
        return externalCodeList;
    }

    public void setExternalCodeList(List<Integer> externalCodeList) {
        this.externalCodeList = externalCodeList;
    }

    public ReferenceGroupService getReferenceGroupDAO() {
        return referenceGroupDAO;
    }

    public void setReferenceGroupDAO(ReferenceGroupService referenceGroupDAO) {
        this.referenceGroupDAO = referenceGroupDAO;
    }

    public List<Long> getObjectCodeGroupList() {
        return objectCodeGroupList;
    }

    public void setObjectCodeGroupList(List<Long> objectCodeGroupList) {
        this.objectCodeGroupList = objectCodeGroupList;
    }

    public void showEditContextMenu(){
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("closable", true);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/trightdialog", options, null);
    }

    public void serviceChange(){
        Integer resourceObjectCode = concreetRightView.gettRightEntityPK().getObjectCode();
        String resourceObjectActions = referenceGroupDAO.findActionByObjectCode(resourceObjectCode);
        if(resourceObjectActions.equals("S")) {
            authorizedActionsList = Arrays.asList(new AuthorizedActions("S – информационные запросы", "S"));
        }
        else if(resourceObjectActions.equals("M")) {
            authorizedActionsList = Arrays.asList(new AuthorizedActions("M – запросы на модификацию", "M"));
        }
        else if(resourceObjectActions.equals("SM") || resourceObjectActions.equals("MS")){
            //authorizedActionsList.clear();
            authorizedActionsList = Arrays.asList(new AuthorizedActions("S – информационные запросы", "S"),
                    new AuthorizedActions("M – запросы на модификацию", "M"),
                    new AuthorizedActions("SM - разрешены и информационные, и запросы на модификацию", "SM"));
        }


    }
}
