package com.ukrcard.ukrapiadminpanel.view;

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.TReferenceGroup;
import com.ukrcard.ukrapiadminpanel.model.service.ExternalSystemService;
import com.ukrcard.ukrapiadminpanel.model.service.ReferenceGroupService;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.sinenko on 01.03.2016.
 */
@ManagedBean(name="referencegroupview")
@SessionScoped
public class ReferenceGroupView implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ManagedProperty(value="#{ReferenceGroupServiceImpl}")
    ReferenceGroupService referenceGroupDAO;

    @ManagedProperty(value="#{trightview}")
            TRightView tRightView;

    List<TReferenceGroup> referenceGroupList;

    TReferenceGroup concreetTReferenceGroup;

    public TRightView gettRightView() {
        return tRightView;
    }

    public void settRightView(TRightView tRightView) {
        this.tRightView = tRightView;
    }

    public ReferenceGroupService getReferenceGroupDAO() {
        return referenceGroupDAO;
    }

    public void setReferenceGroupDAO(ReferenceGroupService referenceGroupDAO) {
        this.referenceGroupDAO = referenceGroupDAO;
    }

    public List<TReferenceGroup> getReferenceGroupList() {
        return referenceGroupList;
    }

    public void setReferenceGroupList(List<TReferenceGroup> referenceGroupList) {
        this.referenceGroupList = referenceGroupList;
    }

    public TReferenceGroup getConcreetTReferenceGroup() {
        return concreetTReferenceGroup;
    }

    public void setConcreetTReferenceGroup(TReferenceGroup concreetTReferenceGroup) {
        this.concreetTReferenceGroup = concreetTReferenceGroup;
    }

    @PostConstruct
    public void init() {
        loadData();
        }

    public void loadData(){
        try{
            referenceGroupList = referenceGroupDAO.findAll();
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    public void close() {
        RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Действие отменено"));
    }

    public void showEditDialog(TReferenceGroup referenceGroup){
        concreetTReferenceGroup = referenceGroup;
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("closable", true);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/referencegroupdialog",options,null);
    }

    public void deleteRecord(TReferenceGroup referenceGroup){
        try {
            referenceGroupDAO.delete(referenceGroup);
            referenceGroupList.remove(referenceGroup);
            loadData();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Успешно удалено"));
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    public void onRowSelect(SelectEvent event) {
        tRightView.concreteReferenceGroup = concreetTReferenceGroup;

        RequestContext.getCurrentInstance().update(":tabview");
        RequestContext.getCurrentInstance().update(":tabview1");
    }


    public boolean saveFocusedRow(){
        concreetTReferenceGroup.setObject(concreetTReferenceGroup.getObject().trim());
        concreetTReferenceGroup.setDescription(concreetTReferenceGroup.getDescription().trim());
        if(concreetTReferenceGroup.getObject().length() != 0) {
            try {
                referenceGroupDAO.save(concreetTReferenceGroup);
                loadData();
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Успешно", "TReferenceGroup сохранён"));
            } catch (DataAccessException err) {
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
            }
        }
        else{
            loadData();
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Одно из обязательных полей не заполнено или заполнено пробелом."));
        }
        return true;
    }

    public void showAddNewDialog() {
        concreetTReferenceGroup = new TReferenceGroup();
        try{
            concreetTReferenceGroup.setObjectCode(referenceGroupDAO.findNextCode());

            Map<String, Object> options = new HashMap<String, Object>();
            options.put("closable", false);
            options.put("resizable", false);
            options.put("contentWidth", 600);
            RequestContext.getCurrentInstance().openDialog("dialogs/referencegroupdialog", options, null);
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    public void onDialogClose(SelectEvent event){
        FacesMessage message=(FacesMessage)event.getObject();
        if(message!=null)
            FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void showEditContextMenu(){
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("closable", false);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/referencegroupdialog", options, null);
    }
}
