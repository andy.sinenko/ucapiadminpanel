package com.ukrcard.ukrapiadminpanel.view;

import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystem;
import com.ukrcard.ukrapiadminpanel.model.entity.ExternalSystemShort;
import com.ukrcard.ukrapiadminpanel.model.entity.TRight;
import com.ukrcard.ukrapiadminpanel.model.entity.TSettingsMpos;
import com.ukrcard.ukrapiadminpanel.model.service.ExternalSystemService;
import com.ukrcard.ukrapiadminpanel.model.service.TRightService;
import com.ukrcard.ukrapiadminpanel.model.service.TSettingsMposService;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jmx.export.annotation.ManagedResource;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.*;
import java.util.ArrayList;

/**
 * Created by a.sinenko on 01.03.2016.
 */
@ManagedBean(name = "tsettingsmposview")
@SessionScoped
public class TSettingsMposView implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ManagedProperty(value = "#{TSettingsMposServiceImpl}")
    TSettingsMposService tSettingsMposDAO;

    @ManagedProperty(value="#{ExternalSystemServiceImpl}")
    private ExternalSystemService externalSystemDAO;

    ExternalSystem concreeteExternalSystem;

    //private List<Integer> externalCodeList;

    List<TSettingsMpos> tSettingsMposList;

    TSettingsMpos concreetTSettingsMpos;

    List<String> PaymentOptionsList = new ArrayList<>();

    @PostConstruct
    public void init() {
        loadData();
    }


    public ExternalSystem getConcreeteExternalSystem() {
        return concreeteExternalSystem;
    }

    public void setConcreeteExternalSystem(ExternalSystem concreeteExternalSystem) {
        this.concreeteExternalSystem = concreeteExternalSystem;
    }

    public List<String> getPaymentOptionsList() {
        return PaymentOptionsList;
    }

    public void setPaymentOptionsList(List<String> paymentOptionsList) {
        PaymentOptionsList = paymentOptionsList;
    }

    public TSettingsMposService gettSettingsMposDAO() {
        return tSettingsMposDAO;
    }

    public void settSettingsMposDAO(TSettingsMposService tSettingsMposDAO) {
        this.tSettingsMposDAO = tSettingsMposDAO;
    }

    public List<TSettingsMpos> gettSettingsMposList() {
        return tSettingsMposList;
    }

    public void settSettingsMposList(List<TSettingsMpos> tSettingsMposList) {
        this.tSettingsMposList = tSettingsMposList;
    }

    public TSettingsMpos getConcreetTSettingsMpos() {
        return concreetTSettingsMpos;
    }

    public void setConcreetTSettingsMpos(TSettingsMpos concreetTSettingsMpos) {
        this.concreetTSettingsMpos = concreetTSettingsMpos;
    }

    public void loadData() {
        //externalCodeList = externalSystemDAO.findExtCodeOrderByExtCode();
        //externalCodeList = externalSystemDAO.findExtCodeAndExtIdOrderByExtCode();
        //tSettingsMposList = tSettingsMposDAO.findAll();
        if(concreeteExternalSystem != null)
            try {
                tSettingsMposList = tSettingsMposDAO.findAndExternalSystem_ExtCode(concreeteExternalSystem.getExtCode());
            } catch (DataAccessException err) {
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
            }
    }

    public void close() {
        RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Действие отменено"));
    }

    public void showEditDialog(TSettingsMpos TSettingsMposItem) {
        concreetTSettingsMpos = TSettingsMposItem;
        String[] temp;
        if(TSettingsMposItem.getOperation()!=null) {
            temp = TSettingsMposItem.getOperation().split(";");
            PaymentOptionsList = Arrays.asList(temp);
        }
        concreetTSettingsMpos.setPsidIsAlwd("0000");

        Map<String, Object> options = new HashMap<String, Object>();
        options.put("closable", true);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/tsettingsmposdialog", options, null);
    }

    public void deleteRecord(TSettingsMpos TSettingsMposItem) {
        try{
            tSettingsMposDAO.delete(TSettingsMposItem);
            tSettingsMposList.remove(TSettingsMposItem);
            loadData();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Успешно удалено"));
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    static String convertToString(List<String> values) {
        if(values.size()>0) {
            StringBuilder builder = new StringBuilder();
            // Append all Integers in StringBuilder to the StringBuilder.
            for (String op : values) {
                builder.append(op);
                builder.append(";");
            }
            // Remove last delimiter with setLength.
            builder.setLength(builder.length() - 1);
            return builder.toString();
        }
        return "";
    }

    public boolean saveFocusedRow() {
        //concreetTSettingsMpos = new TSettingsMpos();
        String PaymentOptions = convertToString(PaymentOptionsList);
        concreetTSettingsMpos.setOperation(PaymentOptions);
        try {
            tSettingsMposDAO.save(concreetTSettingsMpos);
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage("Настройки MPOS сохранёны"));
            loadData();
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
        return true;
    }

    public void showAddNewDialog() {
        if(concreeteExternalSystem != null) {
            concreetTSettingsMpos = new TSettingsMpos();
            Integer extcode = concreeteExternalSystem.getExtCode();
            concreetTSettingsMpos.setExtSystemCode(extcode);
            concreetTSettingsMpos.setPsidIsAlwd("0000");
            try{
                concreetTSettingsMpos.setId(tSettingsMposDAO.findNextCode());

                concreetTSettingsMpos.setIsBusy(0);
                Map<String, Object> options = new HashMap<String, Object>();
                options.put("closable", false);
                options.put("resizable", false);
                options.put("contentWidth", 600);
                RequestContext.getCurrentInstance().openDialog("dialogs/tsettingsmposdialog", options, null);
            } catch (DataAccessException err) {
                RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
            }
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Внимание!", "Для добавления MPOS выберите внешнюю систему на вкладке 'Внешние системы'"));
        }
    }

    public void onDialogClose(SelectEvent event) {
        FacesMessage message = (FacesMessage) event.getObject();
        if (message != null)
            FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public ExternalSystemService getExternalSystemDAO() {
        return externalSystemDAO;
    }

    public void setExternalSystemDAO(ExternalSystemService externalSystemDAO) {
        this.externalSystemDAO = externalSystemDAO;
    }

    /*public List<ExternalSystem> getExternalCodeList() {
        return externalCodeList;
    }

    public void setExternalCodeList(List<ExternalSystem> externalCodeList) {
        this.externalCodeList = externalCodeList;
    }
*/
    public void refreshRightList(Integer extCode)    {
        try{
            List<TSettingsMpos> tempList = tSettingsMposDAO.findAndExternalSystem_ExtCode(extCode);
            if(tempList!=null && tempList.size() > 0)
                this.tSettingsMposList = tempList;
            else
                this.tSettingsMposList = null;
            RequestContext.getCurrentInstance().update("tsettingsmpostable");
        } catch (DataAccessException err) {
            RequestContext.getCurrentInstance().closeDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка", "Ошибка доступа к БД" + err.getMessage()));
        }
    }

    public void showEditContextMenu(){
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("closable", false);
        options.put("resizable", false);
        options.put("contentWidth", 600);
        RequestContext.getCurrentInstance().openDialog("dialogs/tsettingsmposdialog", options, null);
    }
}
