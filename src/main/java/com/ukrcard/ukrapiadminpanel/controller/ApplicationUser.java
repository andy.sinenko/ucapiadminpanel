package com.ukrcard.ukrapiadminpanel.controller;

import com.ukrcard.ukrapiadminpanel.model.entity.Userinfo;
import com.ukrcard.ukrapiadminpanel.model.service.UserinfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

/**
 * Created by a.sinenko on 11.03.2016.
 */

@ManagedBean
@SessionScoped
//@Component
public class ApplicationUser implements Serializable{

    Logger logger = LoggerFactory.getLogger(ApplicationUser.class);

    //@Autowired
    @ManagedProperty(value="#{UserinfoServiceImpl}")
    private UserinfoService userDAO;

    private Userinfo userInfo;

    private static final long serialVersionUID = 1L;
    private int type=0;
    private String login = "guest";
    private String permissions=null;

    public int getType() {return type;}
    public void setType(int type) {        this.type = type;    }


    public String getFormatedName(){
        if(userInfo!=null && userInfo.getUname()!=null && userInfo.getUname().length()>0){
            return userInfo.getUname();
        }else{
            return login;
        }
    }

    public boolean login(String login,String password){

        userInfo = userDAO.login(login, password);

        if(userInfo==null)
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userInfo is null");
        else
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {}", this.userInfo.toString());
        if(this.userInfo==null) {
            this.setLogin("guest");
            this.setPermissions(null);
            return false;
        }else{
            this.setUserInfo(userInfo);
            this.type = Integer.parseInt(userInfo.getType());
            return true;
        }
    }

    @Override
    public String toString() {
        return "ApplicationUser{login:"+login+";permissions:"+permissions+"}";
    }

    public Userinfo getUserInfo() {        return userInfo;    }
    private void setUserInfo(Userinfo userInfo) {
        this.userInfo = userInfo;
        this.login=userInfo.getName();
        this.permissions=userInfo.getPermissions();
        this.type=Integer.parseInt(userInfo.getType());
    }

    public String getLogin() {        return login;    }
    public void setLogin(String login) {        this.login = login;    }

    public String getPermissions() {        return permissions;    }
    public void setPermissions(String permissions) {        this.permissions = permissions;    }


    public UserinfoService getUserDAO() {        return userDAO;    }
    public void setUserDAO( UserinfoService userDAO) {        this.userDAO = userDAO;    }

}
